Squid is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more. It reduces bandwidth and improves response times by caching and reusing frequently-requested web pages. Squid has extensive access controls and makes a great server accelerator. It runs on most available operating systems, including Windows and is licensed under the GNU GPL.

Based on latest squid version: **6.13**

SSLBUMP and almost all the usual options are enabled ...

http://www.squid-cache.org/

https://gitlab.com/fredbcode-images/squid

**fredbcode:**
 
- https://gitlab.com/fredbcode 
- https://github.com/fredbcode

Automated builds of the images are available on:

**E2guardian Docker image:**

https://hub.docker.com/r/fredbcode/e2guardian

**Squid Docker image:**

https://hub.docker.com/r/fredbcode/squid/

**CI description (chatgtp):**

https://e2guardian.numsys.eu/squid-ci/

Latest: https://e2guardian.numsys.eu/squid-ci/chatgpt_analysis_20250304.html

**Quick start**

```
docker run --init -d --name="squid" \
    -v /path/to/yourconfig:/etc/squid \
    -v /path/to/yourlog:/var/log/squid \
    -p 3128:3128 \
    --restart=unless-stopped \
    fredbcode/squid
```
Or with docker-compose

```
docker-compose up -d 
```

**tags**

- x.x -> latest stable version:**PROD**
- build-noprod -> latest build version (for testing purpose only)

**Ports:**

- 3128 proxy standalone

**UID/GUID:**

For security purpose image runs as a non-root user

- 1161

**Persistence**

For the cache to preserve its state should mount a volume /var/spool/squid as volume

**Optionals environment variables**

_If all hosts are down, squid stop_

- supgethosts=https://www.google.com,https://www.facebook.com,https://www.orange.fr,https://www.free.fr

_If ICAP server is down, squid stop_

- supicaphost=e2guardian
- supicapport=1344

_If a file is changed/deleted/created squid reloads_

- autoreload=/etc/squid

**Docker-compose**

https://gitlab.com/fredbcode-images/squid/-/tree/master/docker-compose

Multiple squid proxy instances running on the same host, with load balacing, web & dns filtering (dnsmasq/e2guardian)

**Supported architectures:**

- amd64, armv8

**Where to file issues:**

https://gitlab.com/fredbcode-images/squid

http://www.squid-cache.org/

Docker images for Squid, latest official version 

Arm Raspberry and X86

https://hub.docker.com/r/fredbcode/squid

