FROM debian:bookworm-slim as buildsquid
ARG SQUID_VERSION
ENV URI=${SQUID_VERSION/./_} 
WORKDIR /tmp

RUN echo "deb-src http://deb.debian.org/debian bookworm main contrib" >> /etc/apt/sources.list && apt-get update && apt-get --no-upgrade --no-install-recommends -y build-dep squid && apt-get --no-install-recommends -y install wget tar xz-utils libssl-dev libssl3 winbind ca-certificates \ 
    && wget --progress=dot:giga https://github.com/squid-cache/squid/releases/download/SQUID_$URI/squid-$SQUID_VERSION.tar.gz \
	&& tar -C /tmp --strip-components=1 -xzf squid-$SQUID_VERSION.tar.gz \
	&& ./configure \
        --prefix=/usr \
        --datadir=/usr/share/squid \
		--enable-cache-digests \
		--includedir=/usr/include \
		--program-prefix= \
		--libdir=/usr/lib \
		--libexecdir=/usr/lib/squid \
		--localstatedir=/var \
		--sharedstatedir=/usr/com \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-icap-client \
		--enable-icap-support \
		--enable-async-io \
		--with-pthreads \
    	--with-winbind \
		--bindir=/usr/sbin \
		--sbindir=/usr/sbin \
		--with-squid=/usr/lib/squid \
		--disable-ipv6 \
		--enable-ltdl-convenience \
		--enable-http-violations \
		CFLAGS="-g -O2 -g -Wall -O2" \
		LDFLAGS= \
		CPPFLAGS= \
		CXXFLAGS="-g -O2 -g -Wall -O2" \
		--without-netfilter-conntrack \
		--disable-arch-native \
		--enable-follow-x-forwarded-for \
		--enable-ssl \
		--enable-ssl-crtd \ 
		--with-openssl \
		--enable-storeio="aufs,diskd,ufs" \
		--exec-prefix=/usr \
		--enable-auth-basic="LDAP" \
		--enable-auth-digest="LDAP" \
		--enable-auth-ntlm \
		--enable-auth-negotiate \
        --enable-negotiate-auth-helpers \
	    --with-krb5-config=yes \
		--enable-icap-client \
		--sysconfdir=/etc/squid \
		--with-filedescriptors=48000 \
		--enable-delay-pools \
		--with-large-files \
        --with-pidfile=/squid/squid.pid \
        --with-default-user=squid \
		--enable-removal-policies="lru,heap" \
    && make -j4 \
    && find /tmp/ -type f -name "Makefil*" -delete \
    && find /tmp/ -type f -name "*.in" -delete 
# Install
FROM debian:bookworm-slim
COPY --from=buildsquid --chown=1161 /tmp/src/auth/basic/LDAP/basic_ldap_auth /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/auth/digest/LDAP/digest_ldap_auth /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/auth/negotiate/kerberos/negotiate_kerberos_auth /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/log/DB/log_db_daemon /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/log/file/log_file_daemon /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/unlinkd /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/http/url_rewriters/LFS/url_lfs_rewrite /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/http/url_rewriters/fake/url_fake_rewrite /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/http/url_rewriters/fake/url_fake_rewrite.sh /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/mime.conf.default /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/security/cert_validators/fake/security_fake_certverify /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/security/cert_generators/file/security_file_certgen /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/delayer/ext_delayer_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/SQL_session/ext_sql_session_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/wbinfo_group/ext_wbinfo_group_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/LDAP_group/ext_ldap_group_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/eDirectory_userip/ext_edirectory_userip_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/acl/external/unix_group/ext_unix_group_acl /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/store/id_rewriters/file/storeid_file_rewrite /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/src/DiskIO/DiskDaemon/diskd /usr/lib/squid/ 
COPY --from=buildsquid --chown=1161 /tmp/errors /usr/share/squid/errors 
COPY --from=buildsquid --chown=1161 /tmp/icons /usr/share/squid/icons 
COPY --from=buildsquid --chown=1161 /tmp/src/squid /usr/sbin/ 
COPY --from=buildsquid --chown=1161 /tmp/tools/squidclient/squidclient /usr/sbin/ 
COPY --from=buildsquid --chown=1161 /tmp/src/mime.conf.default /etc/squid/mime.conf 
COPY --from=buildsquid --chown=1161 /tmp/src/squid.conf.default /etc/squid/squid.conf 
RUN adduser --no-create-home --uid 1161 --group --system squid \
    && mkdir -p /usr/share/squid \
    && mkdir -p /usr/lib/squid \
    && mkdir -p /var/log/squid \
    && mkdir -p /var/cache/squid \
    && chown -Rf squid /etc/squid \
    && chown -Rf squid /usr/lib/squid \
    && chown -Rf squid /var/log/squid \
    && chown -Rf squid /var/cache/squid \
    && echo "cache_dir diskd /var/cache/squid/\${service_name} 130 51 51" >> /etc/squid/squid.conf \
    && echo "access_log stdio:/var/log/squid/access.log" >> /etc/squid/squid.conf \
    && echo "cache_log /var/log/squid/cache.log" >> /etc/squid/squid.conf \
    && echo "cache_effective_user squid" >> /etc/squid/squid.conf \
# Removed in 5.8, needed for testing
	&& sed  -i '/http_access deny all/ihttp_access allow localnet' /etc/squid/squid.conf \
	&& sed  -i '/acl localnet src 100.64.0.0\/10/iacl localnet src 127.0.0.1' /etc/squid/squid.conf \
	&& sed  -i '/http_access allow localhost manager/ihttp_access allow localnet manager' /etc/squid/squid.conf \
# Packages dependencies installation for running and clean again 
    && apt-get update && apt-get install -y --no-install-recommends c-icap ca-certificates net-tools rsync libexpat1 libltdl7 libxml2 openssl ca-certificates libldap-common libecap3 libcap2 libdb5.3 libatomic1 dumb-init procps inotify-tools curl \
    && apt-get auto-remove -y && apt-get clean autoclean \
    && rm -rf /var/lib/apt/lists/* && rm -Rf /tmp/* 
COPY --chown=1161 squid /squid
RUN chmod +x /squid/run.sh /squid/autoreload.sh /squid/sup.sh
HEALTHCHECK CMD squidclient -h $HOSTNAME mgr:info | grep "HTTP/1.1 200 OK" || exit 1
EXPOSE 3128
ENTRYPOINT ["dumb-init"]
USER squid
CMD ["bash", "-c", "/squid/run.sh && squid -N -n $HOSTNAME"]
